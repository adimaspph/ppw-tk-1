from django.test import TestCase, Client
from django.urls import reverse, resolve

from django.apps import apps
from findfriends.apps import FindfriendsConfig

from contribute.models import ContributeModel
from .models import Find
from .views import searchfriends, friendlist

# Create your tests here.

# TEST APP
class FindfriendsConfigTest(TestCase):
    def test_app(self):
        self.assertEqual(FindfriendsConfig.name, 'findfriends')
        
class FindTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    # SEARCHFRIENDS   
    # Test url
    def test_url_searchfriends(self):
       response = Client().get('/findfriends/searchfriends/')
       self.assertEqual(response.status_code, 200)
    
    # Test template used
    def test_searchfriends_template(self):
        response = Client().get('/findfriends/searchfriends/')
        self.assertTemplateUsed(response, 'search.html')
    
    # Test views
    def test_views_searchfriends(self):
        found = resolve('/findfriends/searchfriends/')            
        self.assertEqual(found.func, searchfriends)
    
    def test_view_dalam_template_searchfriends(self):
        response = Client().get('/findfriends/searchfriends/') 
        isi_view = response.content.decode('utf8')
        self.assertIn("Carilah temanmu berdasarkan kategori berikut :", isi_view)   
        self.assertIn('<button type="submit" value="Save" class="button"><h4>SEARCH</h4></button>', isi_view)

    # FRIENDLIST    
    # Test url
    def test_url_friendlist(self):
        response = Client().get('/findfriends/friendlist/')
        self.assertEqual(response.status_code, 200)
    
    def test_friendlist_post_url(self):
        response = Client().post('/findfriends/friendlist/', data={'name': 'Ryan Dirga', "interest" : "main bola"})
        amount = ContributeModel.objects.filter(name = "Ryan Dirga").count()
        self.assertEqual(amount, 0)
    
    # Test template used
    def test_friendlist_template(self):
        response = Client().get('/findfriends/friendlist/')
        self.assertTemplateUsed(response, 'list.html')
    
    # Test views
    def test_views_friendlist(self):
        found = resolve('/findfriends/friendlist/')            
        self.assertEqual(found.func, friendlist)
    
    def test_view_dalam_template_friendlist(self):
        response = Client().get('/findfriends/friendlist/') 
        isi_view = response.content.decode('utf8')
        self.assertIn("Hasil Pencarian", isi_view)
        self.assertIn("Semoga ada teman yang sesuai :)", isi_view)

    # Test model      
    def test_check_model_searchfriend(self):
        ContributeModel.objects.create(university="UI", major="Sistem Informasi")
        amount = ContributeModel.objects.all().count()
        self.assertEqual(amount, 1)

    def test_check_model_searchfriend_tahun(self):
        Find.objects.create(tahun=2019)
        amount = Find.objects.all().count()
        self.assertEqual(amount, 1)