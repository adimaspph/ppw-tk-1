from django import forms
from .models import Find
from contribute.models import ContributeModel

class Findform(forms.ModelForm):
    class Meta:
        model = ContributeModel
        fields = ['major', 'university']

        labels = {
            'university' : 'Your friends current / last university', 'major' : 'Your friends current / last major',
        }
        widgets = {
            'university' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'University'}),

            'major' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Major'}),

        }
class TahunForm(forms.ModelForm):
    class Meta:
        model = Find
        fields = ['tahun']

        labels = {
            'tahun' : 'Year of Entry',
        }
