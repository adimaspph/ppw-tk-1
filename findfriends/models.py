from django.db import models

# Create your models here.
TAHUN = [
    ('2020', '2020'),
    ('2019', '2019'),
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('2015', '2015'),
    ('Alumni', 'Alumni'),
]

class Find(models.Model):
    
    tahun = models.CharField(max_length=20, choices=TAHUN)

    