from django.urls import path

from . import views

app_name = 'findfriends'

urlpatterns = [
    path('searchfriends/', views.searchfriends, name='searchfriends'),
    path('friendlist/', views.friendlist, name='friendlist'),
]