from django.shortcuts import render, redirect
from .forms import Findform, TahunForm
from .models import Find
from contribute.models import ContributeModel

# Create your views here.
def searchfriends(request):
    form = Findform()
    formtahun = TahunForm()
    if request.method == "POST":
        form = Findform(request.POST)
        if form.is_valid():
            return redirect('/friendlist/')
    else:
        form = Findform()

    return render(request, 'search.html', {'form' : form, 'formtahun' : formtahun})

def friendlist(request):
    form = Findform(request.POST)
    formtahun = TahunForm(request.POST)
    tahun = formtahun['tahun'].value()
    univ = form['university'].value()
    jurusan = form['major'].value()
    hasil = ContributeModel.objects.filter(university=univ, year_of_entry_cont=str(tahun), major=jurusan)
    return render(request, 'list.html', {'form' : form, 'formtahun' : formtahun, 'hasil' : hasil})

