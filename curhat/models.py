from django.db import models

# Create your models here.
class Curhat(models.Model):
    dari = models.CharField(max_length = 30, default="Anonymous")
    isi = models.TextField()

    def __str__(self):
        return self.dari 