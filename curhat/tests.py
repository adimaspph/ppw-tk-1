from django.test import TestCase, Client
from django.urls import reverse, resolve

# from .models import Curhat
from .views import *
from .urls import *

class CurhatTestCase(TestCase):
    
    # Views test
    def test_views_curhat(self):
        found = resolve('/curhat/')            
        self.assertEqual(found.func, curhat)

    def test_is_curhat_page_exists(self):
        response = Client().get('/curhat/')
        self.assertTemplateUsed(response, 'curhat/home.html')

    

    # Url test
    def test_curhat_create_url_exists(self):
        response = Client().get('/curhat/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_curhat_post_url(self):
        response = Client().post('/curhat/', data={'dari': 'Anton', "isi" : "gk tau isinya"})
        amount = Curhat.objects.filter(dari = "Anton").count()
        self.assertEqual(amount, 1)

    
    # Module test
    def test_check_model_create_curhat(self):
        Curhat.objects.create(dari="Anton", isi="lagi sedih")
        amount = Curhat.objects.all().count()
        self.assertEqual(amount, 1)

    def test_check_model_return_curhat(self):
        Curhat.objects.create(dari="Anton",  isi="lagi sedih")
        result = Curhat.objects.get(id=1)
        self.assertEqual(str(result), "Anton")
