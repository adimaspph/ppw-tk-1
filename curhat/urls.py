from django.urls import include, path
from . import views

app_name = 'curhat'

urlpatterns = [
    path('', views.curhat, name='home'),
]
