from django import forms
from .models import Curhat

# from .models import Matkul
class FormCurhat(forms.ModelForm):
    class Meta:
        model = Curhat
        fields = "__all__"

    dari = forms.CharField(
        max_length=30, required = False, widget=forms.TextInput(attrs = {
        'placeholder': 'Anonymous',
        'type' : 'text',
        }))

    isi = forms.CharField(
        widget=forms.Textarea(attrs = {
        'placeholder': 'Tulis curhatan kamu disini',
        'type' : 'text',
        }))


