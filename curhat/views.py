from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Curhat
from .forms import FormCurhat


# Create your views here.
def curhat(request):
    form = FormCurhat(request.POST or None)
    isi_curhat = Curhat.objects.all()
    response = {
        "curhats": isi_curhat,
        "form_curhat": FormCurhat,
    }

    if (form.is_valid() and request.method == 'POST'):
        form.save()

    return render(request, 'curhat/home.html', response)
