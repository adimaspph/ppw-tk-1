from django.urls import path

from . import views

app_name = 'tanya'

urlpatterns = [
    path('', views.showTanya, name='tanya' )
]