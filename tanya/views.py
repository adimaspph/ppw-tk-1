from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import formTanya
from .models import Tanya

# Create your views here.
def showTanya(request):
    form = formTanya(request.POST or None)
    if (form.is_valid() and request.method =='POST'):
        form.save()
        return HttpResponseRedirect('/tanya')
    pertanyaan = Tanya.objects.all()
    response = {
        'title' : 'Tanya',
        'formTanya' : form,
    }
    return render(request, 'tanya/forms.html', response)