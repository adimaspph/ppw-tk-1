from .models import Tanya
from django import forms
from django.db.models.fields import Field
from django.forms import ModelForm
from django.forms import fields
from django.forms import widgets

class formTanya(forms.ModelForm):
    class Meta:
        model = Tanya
        fields = {
            'nama',
            'pertanyaan',
        }

        widgets = {
            'nama' : forms.TextInput({'placeholder' : 'Masukkan nama anda (Nama samaran diperbolehkan)'}),
            'pertanyaan' : forms.Textarea({'placeholder' : 'Awali pertanyaan dengan "Apa", "Bagaimana", "Mengapa", dll.'})
        }