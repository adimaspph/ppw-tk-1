from django.test import TestCase, Client
from django.urls import reverse, resolve

from .models import Tanya
from .views import showTanya
# Create your tests here.
class TanyaTestCase(TestCase):

    def test_url_tanya(self):
        response = Client().get('/tanya/')
        self.assertEqual(response.status_code, 200)

    def test_tanya_templates(self):
        response = Client().get('/tanya/')
        self.assertTemplateUsed(response, 'tanya/forms.html')

    def test_views_tanya(self):
        asked = resolve('/tanya/')
        self.assertEqual(asked.func, showTanya)