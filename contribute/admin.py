from django.contrib import admin
from contribute.models import ContributeModel, YearOfEntry

admin.site.register(ContributeModel)
admin.site.register(YearOfEntry)
