from django.test import TestCase, Client
from django.urls import reverse, resolve
from contribute.views import lending_page_contribute_view, contributors_view
from contribute.urls import *
from contribute.models import ContributeModel, YearOfEntry
from contribute.form import ContributeForm

class TestContributeApp(TestCase):
    def setUp(self):
        self.year_entry = YearOfEntry.objects.create(year_of_entry = "2010")
        self.contributor = ContributeModel.objects.create(name = "Ani", interest = "Ngoding", 
        major = "ilmu komputer", university = "ui", social_media = "@aninih")
        self.counting_year = YearOfEntry.objects.all().count()
        self.counting_contributor = ContributeModel.objects.all().count()
        self.data = {'image_url': 'image_url', 'name' : "Ani", 'interest' : "Ngoding", 
        'major' : "ilmu komputer", 'university' : "ui", 'year_of_entry_cont': "years", 'social_media' : "@aninih",}

    def test_page_contribute_is_resolved(self):
        url = reverse('lendpagecont')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEquals(resolve(url).func, lending_page_contribute_view)

    def test_page_contributors_is_resolved(self):
        url = reverse('contributors')
        self.assertEquals(resolve(url).func, contributors_view)

    def test_year_of_entry(self):
        self.assertEqual(self.counting_year, 1)

    def test_model_contribute(self):
        self.assertEqual(self.counting_contributor, 1)
        self.assertEqual(self.contributor.__str__(), "Ani")

    def test_form_contribute_view(self):
        tahun_masuk = YearOfEntry.objects.create(year_of_entry = '2010')
        
        response = Client().post('/contribute/', data = {'name' : "Ani", 'interest' : "Ngoding", 
        'major' : "ilmu komputer", 'university' : "ui", 'year_of_entry_cont': tahun_masuk, 'social_media' : "@aninih",})
        amount = ContributeModel.objects.filter(name = "Ani").count()
        self.assertEqual(amount, 1)

    #view test
    def test_view_lending_page_contribute(self):
        form_data = {'name' : "Ani", 'interest' : "Ngoding", 
        'major' : "ilmu komputer", 'university' : "ui", 'year_of_entry_cont': "2015", 'social_media' : "@aninih",}
        form = ContributeForm(data=form_data)
        self.assertTrue(form.is_valid())
        obj = form.save()
        self.assertEqual(obj.name, form_data['name'])
        self.assertEqual(obj.interest, form_data['interest'])
        self.assertEqual(obj.major, form_data['major'])
        self.assertEqual(obj.university, form_data['university'])
        self.assertEqual(obj.year_of_entry_cont, form_data['year_of_entry_cont'])
        self.assertEqual(obj.social_media, form_data['social_media'])
    
    def test_matching_view(self):
        response = Client().get('/contribute/contributors/')
        self.assertTemplateUsed(response, 'contributors.html')
        response2 = Client().get('/contribute/')
        self.assertTemplateUsed(response2, 'contribute.html', 'profile_contribute.html')