from django.forms import ModelForm
from contribute.models import ContributeModel
from django import forms

class ContributeForm(ModelForm):
    class Meta:
        model = ContributeModel
        fields = '__all__'

        widgets = {
            'name' : forms.URLInput(attrs = {'class': 'form-control-lg',
                                            'type': 'url'}),
            'name' : forms.TextInput(attrs = {'class': 'form-control-lg',
                                            'type': 'text',
                                            'placeholder' : 'full name or surname'}),
            'interest' : forms.TextInput(attrs = {'class': 'form-control',
                                            'type': 'text',
                                            'placeholder' : 'e.g. ngoding'}),
            'major' : forms.TextInput(attrs = {'class': 'form-control',
                                            'type': 'text',
                                            'placeholder' : 'current/last major'}),
            'university' : forms.TextInput(attrs = {'class': 'form-control',
                                            'type': 'text',
                                            'placeholder' : 'current/last university'}),
            'year_of_entry_cont' : forms.Select(attrs = {'class' : 'form-control',
                                            'type' : 'option'}),
            'social_media' : forms.TextInput(attrs = {'class': 'form-control',
                                            'type': 'text',
                                            'placeholder' : 'e.g. @kuda_poni (ig)'}),
            
        }
        