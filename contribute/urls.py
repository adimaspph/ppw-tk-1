from django.urls import include, path
from . import views
from contribute.views import lending_page_contribute_view, contributors_view

# , form_contribute_view, profile_contribute_view

urlpatterns = [
    path('', views.lending_page_contribute_view, name='lendpagecont'),
    path('contributors/', views.contributors_view, name = 'contributors'),
]