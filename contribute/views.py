from django.shortcuts import render, redirect
from contribute.form import ContributeForm
from contribute.models import ContributeModel
from django.http import HttpResponseRedirect

def lending_page_contribute_view(request):
    form = ContributeForm()
    if request.method == 'POST':
        form = ContributeForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'profile_contribute.html', {'form_data': form.cleaned_data})   
    context = {'form' : form, "form_contribute": ContributeForm}
    return render(request, 'contribute.html', context)   

def contributors_view(request):
    contributors = ContributeModel.objects.all()

    contributor = {"contributors" : contributors}
    return render(request, 'contributors.html', contributor)
