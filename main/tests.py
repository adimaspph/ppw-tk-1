from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import *
from .urls import *

class LendingPageTestCase(TestCase):
    def test_views_home(self):
        found = resolve('/')            
        self.assertEqual(found.func, home)

    def test_is_lendingpage_exists(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/lendingPage.html')
