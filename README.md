# Tugas Kelompok 1 (E05)

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

Repositori ini berisi berisi proyek Tugas Kelompok 1 yang 
dibuat oleh kelompok E05.

## Daftar Isi
- Nama Anggota Kelompok
- Link Herokuapp
- Cerita Aplikasi dan Manfaatnya
- Daftar Fitur

## Nama Anggota Kelompok

1. Adimas Putra Pratama Hendrata    (1906305575)
2. Affan Mirza Chairy               (1906302516)
3. Khusnul Khotimah                 (1906298986)
4. Rafi Ismail                      (1906302491)
5. Ryan Dirga Aidil Hakim           (1906306741)

## Link Herokuapp

`findterest.herokuapp.com`** atau [klik][link-herokuapp]

## Cerita Aplikasi dan Manfaatnya

Aplikasi yang kami buat bernama **Findterest**. Findterest terdiri dari dua kata, 
yaitu *find* dan *interest*. Jadi melalui aplikasi ini, seseorang dapat
mencari relasi berdasarkan *interest* yang sama dan juga
berbagi pengalaman tentang *interest* nya masing-masing. 

Target user dari Findterest adalah mahasiswa dan freshgraduate. Melalui Findterest, 
seseorang dapat membagi keluh kesahnya selama menjalani perkuliahannya maupun
dalam mencari pekerjaan. 

Kami juga berharap Findterest dapat menjadi wadah untuk berbagi ilmu pengetahuan, 
membantu mahasiswa yang kesulitan mengerjakan tugasnya, dan penyelesaian masalah lain 
melalui fitur yang kami sediakan. 


## Daftar fitur

1. **Find friends**
    User dapat mencari teman yang memiliki persamaan *intereset*, kampus, dan jurusan.
2. **Contribute**
    User dapat berkontribusi kepada Findterest untuk memasukkan profil mereka dan akan kami simpan
    sebagai database. Data ini akan kami jadikan database di fitur **find friends**
3. **Curhat**
    User dapat mencurahkan perasaannya disini. Mereka dapat mencantumkan nama ataupun anonim.
4. **Tanya**
    User dapat bertanya disini. Pertanyaannya dapat seputar perkuliahan, dunia kerja, dan
    hal lain. Sama seperti curhat, mereka dapat mencantumkan nama ataupun anonim. 
5. **Jawab**
    User dapat menjawab pertanyaan yang diajukan user lain. Dalam menjawab juga dapat mencantumkan 
    nama ataupun anonim.


[link-herokuapp]: https://findterest.herokuapp.com
[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[commits-gl]: https://gitlab.com/laymonage/django-template-heroku/-/commits/master