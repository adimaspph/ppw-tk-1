from django.test import TestCase, LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from django.test import TestCase
from tanya.models import Tanya
from .models import *
from .forms import *
from .views import *
from .apps import JawabConfig
import unittest

class JawabConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(JawabConfig.name, 'jawab')
class ModelTest(TestCase):

    def setUp(self):
        tanya = Tanya.objects.create(nama='Jane', pertanyaan = 'Berapakah 2 + 2?')
        self.jawab = Jawab.objects.create(tanya = tanya, nama = 'John', jawaban = 'Jawabannya setauku 4')

    def test_instance_created(self):
        self.assertEqual(Jawab.objects.count(), 1)

    def test_str(self):
        tanya = Tanya.objects.create(nama='Jane', pertanyaan = 'Berapakah 2 + 2?')
        res = Jawab.objects.create(tanya = tanya, nama = 'John', jawaban = 'Jawabannya setauku 4')
        result = Jawab.objects.get(id=1)
        self.assertEqual(str(result), 'John')

class ViewsTest(TestCase):
    def setUp(self):
        tanya = Tanya.objects.create(nama='Jane', pertanyaan = 'Berapakah 2 + 2?')
        self.jawab = Jawab.objects.create(tanya = tanya, nama = 'John', jawaban = 'Jawabannya setauku 4')
        self.jawabs = reverse('jawab:jawabs')
        self.tambahJawaban = reverse('jawab:tambahJawaban', args=[self.jawab.id])
        self.save = reverse('jawab:save', args=[self.jawab.id])
        self.showJawaban = reverse('jawab:showJawaban', args=[self.jawab.id])

    def test_GET_jawabs(self):
        response = self.client.get(self.jawabs)
        self.assertEqual(response.status_code, 200)

    def test_GET_tambahJawaban(self):
        response = self.client.get(self.tambahJawaban)
        self.assertEqual(response.status_code, 200)

    def test_GET_save(self):
        response = self.client.get(self.save)
        self.assertEqual(response.status_code, 302)
    
    def test_GET_showJawaban(self):
        response = self.client.get(self.showJawaban)
        self.assertEqual(response.status_code, 200)