from django.forms.formsets import formset_factory
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from jawab.forms import FormJawaban
from tanya.forms import formTanya
from jawab.models import Jawab
from tanya.models import Tanya

# Create your views here.
def jawabs(request):
    pertanyaan = Tanya.objects.all()
    response = {
        'pertanyaan': pertanyaan,
    }
    return render(request, 'jawab/questions.html', response)

def tambahJawaban(request, id):
    tanya = Tanya.objects.get(id=id)
    response = {
        "form_peserta" : FormJawaban,
        "id": id,
        "pertanyaan": tanya
    }
    return render(request, 'jawab/add.html', response)

def save(request, id=None):
    form = FormJawaban(request.POST or None)
    tanya = Tanya.objects.get(id=id)

    if (request.method == 'POST'):
        nama = form["nama"].value()
        jawaban = form["jawaban"].value()
        ans = Jawab(tanya=tanya, nama=nama, jawaban=jawaban)
        ans.save()
    return HttpResponseRedirect('/jawab/')

def showJawaban(request, id):
    tanya = Tanya.objects.get(id=id)
    jawaban = Jawab.objects.filter(tanya=id)
    response = {
        'pertanyaan' : tanya,
        'jawabans' : jawaban,
    }
    return render(request, 'jawab/show.html', response)