from django.db import models
from tanya.models import Tanya

# Create your models here.

class Jawab(models.Model):
    tanya = models.ForeignKey(Tanya, on_delete=models.CASCADE, null=True)
    nama = models.CharField(max_length=150)
    jawaban = models.CharField(max_length=1000)

    def __str__(self):
        return self.nama