from django.urls import path

from . import views

app_name = 'jawab'

urlpatterns = [
    path('', views.jawabs, name='jawabs'),
    path('tambahJawaban/<int:id>', views.tambahJawaban, name='tambahJawaban'),
    path('tambahJawaban/save/<int:id>', views.save, name='save'),
    path('showJawaban/<int:id>', views.showJawaban, name='showJawaban')
]