from django.db.models.fields import Field
from django.forms import ModelForm, fields, widgets
from jawab.models import Jawab
from django import forms

class FormJawaban(ModelForm):
    class Meta:
        model = Jawab
        fields = ['nama', 'jawaban']

        widgets = {
            'nama' : forms.TextInput({'placeholder':'Masukkan namamu'}),
            'jawaban' : forms.Textarea({'placeholder' : 'Jawab pertanyaan disini'})
        }